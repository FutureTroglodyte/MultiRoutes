"""
MultiRoutes
"""

import folium
import pandas as pd
import streamlit as st
from pypolyline.cutil import decode_polyline
from streamlit_folium import st_folium

from maps import create_map
from solve_vrp import initialize_vroom_backend, solve_vrp


def main():
    with st.sidebar:
        st.title("Sidebar")
        # Add more here

    ### Head Row ###

    col_11, col_12 = st.columns([1, 1])

    with col_11:
        st.title("Multiroutes")
        # Add more here

    with col_12:
        st.write("Add more here")

    ### Body ###

    tab_1, tab_2 = st.tabs(["Tab_1", "Tab_2"])

    with tab_1:
        st.title("Tab_1")
        st_folium(
            fig=fig,
            height=900,
            width=1600,
        )

    # with tab_2:
    #     st.title("Tab_2")
    #     # st.map()
    #     # Add more here


if __name__ == "__main__":
    # Run with `$ streamlit run app.py`

    ### Streamlit Config ###

    st.set_page_config(
        page_title="MultiRoute",
        page_icon=":rocket:",
        layout="wide",
        initial_sidebar_state="collapsed",
    )

    ### Solve Example VRP ###

    vroom_docker = initialize_vroom_backend()

    res = solve_vrp(
        vroom_docker=vroom_docker,
        vrp_json_file="example.json",
        provide_detailed_routes=True,
    )

    df_routes = pd.DataFrame(res["routes"])

    ### Generate Example Map ###

    steps_vehicle_0 = [
        (x["location"][1], x["location"][0])
        for x in res["routes"][0]["steps"]
        if "location" in x.keys()
    ]
    route_vehicle_0 = [
        (b, a) for a, b in decode_polyline(str.encode(res["routes"][0]["geometry"]), 5)
    ]

    steps_vehicle_1 = [
        (x["location"][1], x["location"][0])
        for x in res["routes"][1]["steps"]
        if "location" in x.keys()
    ]
    route_vehicle_1 = [
        (b, a) for a, b in decode_polyline(str.encode(res["routes"][1]["geometry"]), 5)
    ]

    fig = create_map(
        data=[
            (steps_vehicle_0, route_vehicle_0, "Vehicle 0"),
            (steps_vehicle_1, route_vehicle_1, "Vehicle 1"),
        ]
    )

    main()

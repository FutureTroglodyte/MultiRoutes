"""Solve Vehicle Routing Problem with VROOM (docker)"""

import json
import os

import docker
import pandas as pd
from docker.models.containers import Container


def initialize_vroom_backend() -> Container:
    """Simply calls docker compose up -d and init vroom container"""
    os.system("docker compose up -d")
    client = docker.from_env()
    return client.containers.get("vroom")


def solve_vrp(
    vroom_docker: Container,
    vrp_json_file: str,
    vrp_volume: str = "VRPs/",
    provide_detailed_routes: bool = False,
) -> dict:
    """Solve Vehicle Routing Problem with VROOM (docker)"""
    if provide_detailed_routes:
        command = f"vroom -i {vrp_volume + vrp_json_file} -g"
    else:
        command = f"vroom -i {vrp_volume + vrp_json_file}"

    return json.loads(vroom_docker.exec_run(cmd=command).output)


if __name__ == "__main__":
    vroom_docker = initialize_vroom_backend()

    res = solve_vrp(
        vroom_docker=vroom_docker,
        vrp_json_file="example.json",
        provide_detailed_routes=False,
    )

    df_routes = pd.DataFrame(res["routes"])

    print(df_routes.head())

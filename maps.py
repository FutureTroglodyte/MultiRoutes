"""
Mapping functions


tiles
"OpenStreetMap"
"Stamen Terrain"
"Stamen Toner"
"Stamen Watercolor"
"CartoDB positron"
"CartoDB dark_matter"

"""


import folium


def create_map(data) -> folium.Figure:
    """dummy create map function"""
    # fig = folium.Figure(width=800, height=450)
    fig = folium.Figure()
    m = folium.Map(
        location=[48.71764, 2.35044], zoom_start=11, tiles="CartoDB dark_matter"
    ).add_to(fig)
    folium.raster_layers.TileLayer()
    for steps, route, vehicle in data:
        [folium.Marker(location=step).add_to(m) for step in steps]
        folium.PolyLine(locations=route, tooltip=vehicle).add_to(m)
    return fig

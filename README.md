# MultiRoutes

## Start Vroom Backend

``` sh
$ docker compose up -d
```

## Setup Instructions

### OSRM Backend

Run:

``` sh
$ mkdir osrm
$ cd osrm
$ wget http://download.geofabrik.de/europe/germany/ile-de-france-latest.osm.pbf
$ docker run -t -v "${PWD}:/data" ghcr.io/project-osrm/osrm-backend osrm-extract -p /opt/car.lua /data/ile-de-france-latest.osm.pbf
$ docker run -t -v "${PWD}:/data" ghcr.io/project-osrm/osrm-backend osrm-partition /data/ile-de-france-latest.osrm
$ docker run -t -v "${PWD}:/data" ghcr.io/project-osrm/osrm-backend osrm-customize /data/ile-de-france-latest.osrm
```

Source: https://github.com/Project-OSRM/osrm-backend
